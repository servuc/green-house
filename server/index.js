const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8080 });

let myScores = [];
const myMaxScoresList = 5;

wss.on('connection', function connection(ws) {
    console.log( "New player" );
    ws.on('message', function incoming(message) {
        let myJson = JSON.parse( message );
        if( myJson.score !== undefined && ! Number.isNaN( Number.parseInt( myJson.score ) ) && myJson.pseudo !== undefined && myJson.mode !== undefined &&
            ( myJson.mode === 'a' || myJson.mode === 'b' )
        ) {
            let myScore =  Number.parseInt( myJson.score );
            if( 0 <= myScore && myScore <= 999 ) {
                myScores.push( { score: myScore, pseudo: myJson.pseudo, mode: myJson.mode } );
                myScores.sort( (scoreA, scoreB ) => scoreB.score - scoreA.score );
            }
        }

        let myScoreA = 0;
        let myScoreB = 0;
        let myMiniScores = JSON.stringify( myScores.filter( (val) => {
            if( val.mode === 'a' && myScoreA < myMaxScoresList ) {
                myScoreA++;
                return true;
            }
            if( val.mode === 'b' && myScoreB < myMaxScoresList ) {
                myScoreB++;
                return true;
            }
        } ) );
        wss.clients.forEach(value => {
            value.send( myMiniScores );
        })
    });

    let myScoreA = 0;
    let myScoreB = 0;
    let myMiniScores = JSON.stringify( myScores.filter( (val) => {
        if( val.mode === 'a' && myScoreA < myMaxScoresList ) {
            myScoreA++;
            return true;
        }
        if( val.mode === 'b' && myScoreB < myMaxScoresList ) {
            myScoreB++;
            return true;
        }
    } ) );
    ws.send( myMiniScores );
});
