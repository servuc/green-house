"use strict";

const SCORE_WS_URL = "ws://localhost:8080";

class Score {
    constructor() {
        this.ulDom = document.getElementById("score_list");
        this.ws = new WebSocket( SCORE_WS_URL );

        this.ws.onmessage = (event) => {
            if( event.data !== undefined && event.data !== null ) {
                this.setScore( JSON.parse(event.data) );
            }
        };

        this.ws.onclose = () => {
            this.clear();
            let myLiDom = document.createElement("li");
            myLiDom.innerHTML = '<b>Server disconnect</b>';
            this.ulDom.append( myLiDom );
        };

        this.ws.onerror = () => {
            this.clear();
            let myLiDom = document.createElement("li");
            myLiDom.innerHTML = '<b>Can\'t connect scoring server</b>';
            this.ulDom.append( myLiDom );
        }
    }

    clear() {
        while ( this.ulDom.firstChild ) {
            this.ulDom.removeChild(this.ulDom.firstChild);
        }
    }

    setScore( data ) {
        this.clear();

        if( data.length === 0 ) {
            let myLiDom = document.createElement("li");
            myLiDom.innerHTML = '<b>No score send to server</b>';
            this.ulDom.append( myLiDom );
        }
        else {
            for (let i = 0; i < data.length; i++) {
                if( data[i].mode === 'a' ) {
                    let myLiDom = document.createElement("li");
                    myLiDom.innerHTML = '<b>' + data[i].pseudo + '</b> : <i>' + data[i].score + '</i> (A)';
                    this.ulDom.append( myLiDom );
                }
            }
            for (let i = 0; i < data.length; i++) {
                if( data[i].mode === 'b' ) {
                    let myLiDom = document.createElement("li");
                    myLiDom.innerHTML = '<b>' + data[i].pseudo + '</b> : <i>' + data[i].score + '</i> (B)';
                    this.ulDom.append( myLiDom );
                }
            }
        }
    }

    sendScore( pseudo, score, mode ) {
        if( this.ws.readyState === WebSocket.OPEN ) {
            this.ws.send( JSON.stringify( {
                pseudo: pseudo,
                score: score,
                mode: mode
            }));
        }
        alert("Il y a des crèpes dans le bureau de Green ;)");
    }
}
