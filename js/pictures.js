"use strict";

class Pictures {
    static gets () {
        return [
            {
                id: "spider_left",
                css: 'bottom',
                pictures : [
                    "/img/section/bottom/araignee_gauche_a.png",
                    "/img/section/bottom/araignee_gauche_b.png",
                    "/img/section/bottom/araignee_gauche_c.png",
                    "/img/section/bottom/araignee_gauche_d.png",
                    "/img/section/bottom/araignee_gauche_e.png"
                ]
            },
            {
                id: "spider_right",
                css: 'bottom',
                pictures : [
                    "/img/section/bottom/araignee_droite_a.png",
                    "/img/section/bottom/araignee_droite_b.png",
                    "/img/section/bottom/araignee_droite_c.png",
                    "/img/section/bottom/araignee_droite_d.png",
                    "/img/section/bottom/araignee_droite_e.png"
                ]
            },
            {
                id: "man_bottom",
                css: 'bottom',
                pictures : [
                    "/img/section/bottom/bonhomme_bas_a.png",
                    "/img/section/bottom/bonhomme_bas_b1.png",
                    "/img/section/bottom/bonhomme_bas_b2.png",
                    "/img/section/bottom/bonhomme_bas_b3.png",
                    "/img/section/bottom/bonhomme_bas_c.png"
                ]
            },
            {
                id: "gaz_bottom_left",
                css: 'bottom',
                pictures : [,
                    "/img/section/bottom/gaz_gauche_e.png",
                    "/img/section/bottom/gaz_gauche_d.png",
                    "/img/section/bottom/gaz_gauche_c.png",
                    "/img/section/bottom/gaz_gauche_b.png",
                    "/img/section/bottom/gaz_gauche_a.png"
                ]
            },
            {
                id: "gaz_bottom_right",
                css: 'bottom',
                pictures : [
                    "/img/section/bottom/gaz_droite_e.png",
                    "/img/section/bottom/gaz_droite_d.png",
                    "/img/section/bottom/gaz_droite_c.png",
                    "/img/section/bottom/gaz_droite_b.png",
                    "/img/section/bottom/gaz_droite_a.png"
                ]
            },
            {
                id: "flower_2",
                css: 'bottom',
                pictures : [
                    "/img/section/bottom/plante_gauche_a.png",
                    "/img/section/bottom/plante_gauche_b.png"
                ]
            },
            {
                id: "flower_3",
                css: 'bottom',
                pictures : [
                    "/img/section/bottom/plante_droite_a.png",
                    "/img/section/bottom/plante_droite_b.png"
                ]
            },

            {
                id: "flower_0",
                css: 'top',
                pictures : [
                    "/img/section/top/plante_gauche_a.png",
                    "/img/section/top/plante_gauche_b.png"
                ]
            },
            {
                id: "flower_1",
                css: 'top',
                pictures : [
                    "/img/section/top/plante_droite_a.png",
                    "/img/section/top/plante_droite_b.png"
                ]
            },
            {
                id: "gaz_top_left",
                css: 'top',
                pictures : [
                    "/img/section/top/gaz_b.png",
                    "/img/section/top/gaz_a.png"
                ]
            },
            {
                id: "gaz_top_right",
                css: 'top',
                pictures : [
                    "/img/section/top/gaz_c.png",
                    "/img/section/top/gaz_d.png"
                ]
            },
            {
                id: "man_top",
                css: 'top',
                pictures : [
                    "/img/section/top/bonhomme_a.png",
                    "/img/section/top/bonhomme_b.png",
                    "/img/section/top/bonhomme_c.png",
                    "/img/section/top/bonhomme_d.png",
                    "/img/section/top/bonhomme_e.png"
                ]
            },
            {
                id: "miss",
                css: 'top',
                pictures : [
                    "/img/section/top/miss.png",
                    "/img/section/top/miss_a.png",
                    "/img/section/top/miss_b.png",
                    "/img/section/top/miss_c.png"
                ]
            },
            {
                id: "bug_left",
                css: 'top',
                pictures : [
                    "/img/section/top/vers_e.png",
                    "/img/section/top/vers_d.png",
                    "/img/section/top/vers_c.png",
                    "/img/section/top/vers_b.png",
                    "/img/section/top/vers_a.png"
                ]
            },
            {
                id: "bug_right",
                css: 'top',
                pictures : [
                    "/img/section/top/vers_f.png",
                    "/img/section/top/vers_g.png",
                    "/img/section/top/vers_h.png",
                    "/img/section/top/vers_i.png",
                    "/img/section/top/vers_j.png"
                ]
            },
            {
                id: "score_1",
                css: 'top',
                pictures : [
                    "/img/section/top/score/num_a_a.png",
                    "/img/section/top/score/num_a_b.png",
                    "/img/section/top/score/num_a_c.png",
                    "/img/section/top/score/num_a_d.png",
                    "/img/section/top/score/num_a_e.png",
                    "/img/section/top/score/num_a_f.png",
                    "/img/section/top/score/num_a_g.png"
                ]
            },
            {
                id: "score_2",
                css: 'top',
                pictures : [
                    "/img/section/top/score/num_b_a.png",
                    "/img/section/top/score/num_b_b.png",
                    "/img/section/top/score/num_b_c.png",
                    "/img/section/top/score/num_b_d.png",
                    "/img/section/top/score/num_b_e.png",
                    "/img/section/top/score/num_b_f.png",
                    "/img/section/top/score/num_b_g.png"
                ]
            },
            {
                id: "score_3",
                css: 'top',
                pictures : [
                    "/img/section/top/score/num_c_a.png",
                    "/img/section/top/score/num_c_b.png",
                    "/img/section/top/score/num_c_c.png",
                    "/img/section/top/score/num_c_d.png",
                    "/img/section/top/score/num_c_e.png",
                    "/img/section/top/score/num_c_f.png",
                    "/img/section/top/score/num_c_g.png"
                ]
            },
            {
                id: "score_4",
                css: 'top',
                pictures : [
                    "/img/section/top/score/num_d_a.png",
                    "/img/section/top/score/num_d_b.png",
                    "/img/section/top/score/num_d_c.png",
                    "/img/section/top/score/num_d_d.png",
                    "/img/section/top/score/num_d_e.png",
                    "/img/section/top/score/num_d_f.png",
                    "/img/section/top/score/num_d_g.png"
                ]
            },
            {
                id: "time_sep",
                css: 'top',
                pictures : [
                    "/img/section/top/score/time_sep.png"
                ]
            }
        ];
    }
}
