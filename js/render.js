"use strict";

class Render {
    constructor( game ) {
        this.loadPictures();
        this.fps = 60;
        this.game = game;
        this.isRendering = false;
        this.interval = window.setInterval( () => {
            if( ! this.isRendering ) {
                this.isRendering = true;

                //Render functions
                if( this.game.mode !== undefined ) {
                    this.displayScore();
                    this.displayMisses();
                    this.displayPlayer();
                    this.displaySpiders();
                    this.displayBugs();
                    this.displayFlowers();
                    this.displaySprays();
                }
                else {
                    this.displayTime();
                }

                this.isRendering = false;
            }
        }, 1000 / this.fps );
    }

    loadPictures() {
        let myGameDiv = document.getElementById('game');
        Pictures.gets().forEach( function( section ) {
            let myCounter = 0;
            section.pictures.forEach( function ( path ) {
                let myItem = document.createElement('img');
                myItem.classList.add( 'absolute', 'relative-' + section.css );
                myItem.setAttribute('src', path);
                myItem.setAttribute("id", section.id + '_' + myCounter);
                myItem.style.display = 'none';
                myCounter++;
                myGameDiv.append( myItem );
            });
        });
    }

    displayScore() {
        document.getElementById( 'time_sep_0' ).style.display = 'none';
        let myScore = ((this.game.score + 10000) + "").substr( 1 );
        for( let i = 0; i < myScore.length; i++ ) {
            Render.renderNumber( i + 1, parseInt( myScore.charAt( i ) ) );
        }
    }

    /**
     * Show/hide player
     * @type {number}
     *
     * 5 6 7 8 9
     *     3
     *     2
     *   0 1 4
     */
    displayPlayer() {
        for( let i = 0; i < 5; i++ ) {
            document.getElementById( 'man_bottom_' + i ).style.display = 'none';
            document.getElementById( 'man_top_' + i ).style.display = 'none';
        }

        if( this.game.playerPosition < 5 ) {
            document.getElementById( 'man_bottom_' + this.game.playerPosition ).style.display = 'block';
        }
        else {
            document.getElementById( 'man_top_' + (this.game.playerPosition - 5) ).style.display = 'block';
        }
    }

    displayTime() {
        let myDate = new Date();
        document.getElementById( 'time_sep_0' ).style.display = ( myDate.getMilliseconds() > 500 ) ? 'block' : 'none';

        Render.renderNumber( 1, (myDate.getHours() - (myDate.getHours() % 10)) / 10 );
        Render.renderNumber( 2, myDate.getHours() % 10 );
        Render.renderNumber( 3, (myDate.getMinutes() - (myDate.getMinutes() % 10)) / 10 );
        Render.renderNumber( 4, myDate.getMinutes() % 10 );
    }

    displaySpiders() {
        for ( let i = 0; i < 5; i++) {
            document.getElementById( 'spider_left_' + i ).style.display = 'none';
            document.getElementById( 'spider_right_' + i ).style.display = 'none';
        }

        for ( let i = 0; i < this.game.spiders.length; i++) {
            if( this.game.spiders[i].pos >= 0 ) {
                document.getElementById( (this.game.spiders[i].side ? MONSTER_SPIDER_LEFT : MONSTER_SPIDER_RIGHT) + '_' + this.game.spiders[i].pos ).style.display = 'block';
            }
        }
    }

    displayBugs() {
        for ( let i = 0; i < 5; i++) {
            document.getElementById( 'bug_left_' + i ).style.display = 'none';
            document.getElementById( 'bug_right_' + i ).style.display = 'none';
        }

        for ( let i = 0; i < this.game.bugs.length; i++) {
            document.getElementById( (this.game.bugs[i].side ?  MONSTER_BUG_LEFT : MONSTER_BUG_RIGHT ) + '_' + this.game.bugs[i].pos ).style.display = 'block';
        }
    }

    static renderNumber(position, value ) {
        //   a
        // g   b
        //   c
        // f   d
        //   e
        let myPositions;
        switch( value ) {
            case 0 :
                myPositions = [ true, true, false, true, true, true, true ];
                break;
            case 1 :
                myPositions = [ false, true, false, true, false, false, false ];
                break;
            case 2 :
                myPositions = [ true, true, true, false, true, true, false ];
                break;
            case 3 :
                myPositions = [ true, true, true, true, true, false, false ];
                break;
            case 4 :
                myPositions = [ false, true, true, true, false, false, true ];
                break;
            case 5 :
                myPositions = [ true, false, true, true, true, false, true ];
                break;
            case 6 :
                myPositions = [ true, false, true, true, true, true, true ];
                break;
            case 7 :
                myPositions = [ true, true, false, true, false, false, false ];
                break;
            case 8 :
                myPositions = [ true, true, true, true, true, true, true ];
                break;
            case 9 :
                myPositions = [ true, true, true, true, true, false, true ];
                break;
        }

        for( let i = 0; i < myPositions.length; i++ ) {
            document.getElementById( 'score_' + position + '_' + i ).style.display = ( myPositions[ i ] ) ? 'block' : 'none';
        }
    }

    displayMisses() {
        for( let i = 0; i < 4; i++ ) {
            document.getElementById( 'miss_' + i ).style.display = ( i <= this.game.misses ) ? 'block' : 'none';
        }
    }

    displayFlowers() {
        for( let i = 0; i < 4; i++ ) {
            document.getElementById( 'flower_' + i + '_0' ).style.display = ( this.game.deadFlower === i ) ? 'none' : 'block';
            document.getElementById( 'flower_' + i + '_1' ).style.display = ( this.game.deadFlower === i ) ? 'block' : 'none';
        }
    }

    displaySprays() {
        for( let i = 0; i < 2; i++ ) {
            document.getElementById( 'gaz_top_left_' + i ).style.display = 'none';
            document.getElementById( 'gaz_top_right_' + i ).style.display = 'none';
        }

        for( let i = 0; i < 5; i++ ) {
            document.getElementById( 'gaz_bottom_left_' + i ).style.display = 'none';
            document.getElementById( 'gaz_bottom_right_' + i ).style.display = 'none';
        }

        for( let i = 0; i < this.game.sprays.length; i++ ) {
            document.getElementById( this.game.sprays[ i ].getPictureId() ).style.display = 'block';
        }
    }
}
