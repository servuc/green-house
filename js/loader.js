document.addEventListener('DOMContentLoaded', function () {
    try {
        "use strict";
        class TestIfYourBrowserIsTooOld{}

        let myScripts = [
            "/js/render.js",
            "/js/pictures.js",
            "/js/input.js",
            "/js/spray.js",
            "/js/score.js",
            "/js/game.js"
        ];
        let myScriptCounter = 0;

        myScripts.forEach( function( item, index ) {
            let myScript = document.createElement("script");
            myScript.setAttribute('src', item);
            myScript.addEventListener("load", function(){
                myScriptCounter++;
                if( myScripts.length === myScriptCounter) {
                    new Game();
                }
            });
            document.body.append( myScript );
        });
    } catch (e) {
        console.log(e);
        document.getElementById("popup-too-old").classList.add('popup-block-open');
    }
});
