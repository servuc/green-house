"use strict";

let SPRAY_FORCE_END_LIFE = -1;
let SPIDER_NO_KILL = null;

class Spray {
    constructor(game, position, side, step) {
        this.game = game;
        this.counter = step * this.getStepCount();

        //position true = top, false = bottom
        //side true = left, false = right
        this.position = position;
        this.side = side;
    }

    getPictureId() {
        return 'gaz_' + ( ( this.position ) ? 'top_' : 'bottom_' ) + ( ( this.side ) ? 'left_' : 'right_' ) + ( (this.counter - (this.counter % this.getStepCount()) ) / this.getStepCount());
    }

    compare( position, side, step ) {
        return this.position === position && this.side === side && ( this.counter - ( this.counter % this.getStepCount() ) ) / this.getStepCount() === step;
    }

    getStepCount() {
       return ( this.game.mode === GAME_MODE_A ) ? GAME_MODE_A_SPRAY_LIFE_COUNTER : GAME_MODE_B_SPRAY_LIFE_COUNTER;
    }

    isKillingBugs() {
        let myKilledBugs = [];
        let mySprayPos = ( this.counter - ( this.counter % this.getStepCount() ) ) / this.getStepCount();
        for( let i = 0; i < this.game.bugs.length; i++ ) {
            if( this.position === TOP && this.game.bugs[ i ].side === this.side && (
                ( [ 1, 2 ].indexOf( this.game.bugs[ i ].pos ) !== NO_INDEX_OF && mySprayPos === 0 ) ||
                ( [ 3, 4 ].indexOf( this.game.bugs[ i ].pos ) !== NO_INDEX_OF && mySprayPos === 1 )
            ) ) {
                this.counter = SPRAY_FORCE_END_LIFE;
                myKilledBugs.push( this.game.bugs[ i ].index );
            }
        }

        return  myKilledBugs;
    }

    isKillingSpider() {
        let myKilledSpider = SPIDER_NO_KILL;
        for (let i = 0; i < this.game.spiders.length; i++) {
            if( this.game.spiders[ i ].side === this.side && this.position === BOTTOM ) {
                if( this.game.spiders[ i ].pos === 4 ) {
                    myKilledSpider = this.game.spiders[ i ].index;
                    this.counter = SPRAY_FORCE_END_LIFE;
                }
                else if( this.game.spiders[ i ].pos ===  4 - ( ( this.counter - ( this.counter % this.getStepCount() ) ) / this.getStepCount() ) ) {
                    myKilledSpider = this.onForwardingSpider( i );
                    if( myKilledSpider !== SPIDER_NO_KILL ) {
                        this.counter = SPRAY_FORCE_END_LIFE;
                    }
                }
                break;
            }
        }
        return myKilledSpider;
    }

    onForwardingSpider( i ) {
        let myKilledSpider = SPIDER_NO_KILL;
        if( --this.game.spiders[ i ].pos < 0 ) {
            myKilledSpider = this.game.spiders[ i ].index;
        }
        else {
            myKilledSpider = -this.game.spiders[ i ].index;
        }
        return myKilledSpider;
    }

    onEffectAndShouldDie() {
        if( this.counter === SPRAY_FORCE_END_LIFE ) {
            return true;
        }

        this.counter++;
        if( ( this.position === TOP && ( this.counter === this.getStepCount() || this.counter >= this.getStepCount() * 2 ) ) ) {
            return true;
        }
        return this.counter >= this.getStepCount() * 5;
    }
}
