"use strict";

class Input {

    constructor(game) {
        this.game = game;
        let that = this;
        document.addEventListener('keydown', function(event) {
            that.onKeyForMode( event.code )
            that.onKeyForSpray( event.code )
            that.onKeyForManMove( event.code )
        });
    }

    onKeyForMode( keyCode ) {
        if( keyCode === "KeyO" ) {
            this.game.launch( GAME_MODE_A );
        }
        else if( keyCode === "KeyP" ) {
            this.game.launch( GAME_MODE_B );
        }
    }

    onKeyForSpray( keyCode ) {
        if( keyCode === "Digit0" || keyCode === "Space" ) {
            this.game.onSpray();
        }
    }

    onKeyForManMove( keyCode ) {
        //Left
        if( keyCode === "KeyA" || keyCode === "ArrowLeft" ) {
            if( [ 1, 6, 7, 8, 9].indexOf( this.game.playerPosition ) !== NO_INDEX_OF ) {
                this.game.playerPosition -= 1;
            }
            else if( this.game.playerPosition === 4 ) {
                this.game.playerPosition = 1;
            }
        }
        //Right
        else if( keyCode === "KeyD" || keyCode === "ArrowRight" ) {
            if( [ 0, 5, 6, 7, 8 ].indexOf( this.game.playerPosition ) !== NO_INDEX_OF ) {
                this.game.playerPosition += 1;
            }
            else if( this.game.playerPosition === 1 ) {
                this.game.playerPosition = 4;
            }
        }
        //Up
        else if( keyCode === "KeyW" || keyCode === "ArrowUp" ) {
            if( [ 1, 2 ].indexOf( this.game.playerPosition ) !== NO_INDEX_OF ) {
                this.game.playerPosition += 1;
            }
            else if( this.game.playerPosition === 3 ) {
                this.game.playerPosition = 7;
            }
        }
        //Down
        else if( keyCode === "KeyS" || keyCode === "ArrowDown" ) {
            if( [ 2, 3 ].indexOf( this.game.playerPosition ) !== NO_INDEX_OF ) {
                this.game.playerPosition -= 1;
            }
            else if( this.game.playerPosition === 7 ) {
                this.game.playerPosition = 3;
            }
        }
    }
}
