"use strict";

let GAME_MODE_A = 'a';
let GAME_MODE_B = 'b';

let GAME_MODE_A_GAME_INTERVAL = 550;
let GAME_MODE_B_GAME_INTERVAL = 350;

let GAME_MODE_BUG_SPAWN = 0.4;
let GAME_MODE_SPIDER_SPAWN = 0.3;

let GAME_MODE_A_MONSTER_MOVE_COUNTER = 3;
let GAME_MODE_B_MONSTER_MOVE_COUNTER = 3;

let GAME_MODE_A_SPRAY_LIFE_COUNTER = 2;
let GAME_MODE_B_SPRAY_LIFE_COUNTER = 2;

let FLOWER_NOT_DEAD = null;
let FLOWER_MAX_DEAD = 3;

let MONSTER_MAX_POS = 5;

let SCORE_MAX = 999;

let MONSTER_SPIDER_LEFT = 'spider_left';
let MONSTER_SPIDER_RIGHT = 'spider_right';

let MONSTER_BUG_LEFT = 'bug_left';
let MONSTER_BUG_RIGHT = 'bug_right';

let TOP = true;
let BOTTOM = false;
let LEFT = true;
let RIGHT = false;

let NO_INDEX_OF = -1;

let PLAYER_VS_SPRAY = [ null, null, null, null, null, null, null, null, null, null ];
PLAYER_VS_SPRAY[ 5 ] = { position: true, side: true, step: 1 };
PLAYER_VS_SPRAY[ 6 ] = { position: true, side: true, step: 0 };
PLAYER_VS_SPRAY[ 8 ] = { position: true, side: false, step: 0 };
PLAYER_VS_SPRAY[ 9 ] = { position: true, side: false, step: 1 };
PLAYER_VS_SPRAY[ 0 ] = { position: false, side: true, step: 0 };
PLAYER_VS_SPRAY[ 4 ] = { position: false, side: false, step: 0 };

class Game {

    constructor() {
        this.indexSpiders = 1;
        this.indexBugs = 1;
        this.mode = undefined;
        this.misses = 0;
        this.score = 0;
        this.interval = 0;
        this.spiders = [];
        this.bugs = [];
        this.deadFlower = -1;
        this.deadCounter = 0;
        this.sprays = [];
        this.isScoreUpperThan300 = false;
        this.registredPseudo = '';

        /**
         * Position of man in game
         * @type {number}
         *
         * 5 6 7 8 9
         *     3
         *     2
         *   0 1 4
         */
        this.playerPosition = 1;

        this.render = new Render( this );
        this.input = new Input( this );
        this.ws = new Score();
    }

    launch( mode ) {
        let that = this;
        if( this.mode === undefined ) {
            this.mode = mode;
            this.score = 0;
            this.interval = window.setInterval(  () => {
                if( this.deadFlower !== FLOWER_NOT_DEAD ) {
                    this.deadFlower = FLOWER_NOT_DEAD;
                }
                else {
                    that.onSpraysEffects();
                    that.onSpiderSpawn();
                    that.onBugSpawn();
                    that.onSpidersMove();
                    that.onBugsMove();
                }
            }, this.mode === GAME_MODE_A ? GAME_MODE_A_GAME_INTERVAL : GAME_MODE_B_GAME_INTERVAL );
        }
    }

    onSpray() {
        if( PLAYER_VS_SPRAY.length > this.playerPosition && PLAYER_VS_SPRAY[ this.playerPosition ] != null ) {
            let mySprayFound = false;
            for ( let i = 0; i < this.sprays; i++) {
                if( this.sprays[i].pos.compare( PLAYER_VS_SPRAY[ this.playerPosition ].position,  PLAYER_VS_SPRAY[ this.playerPosition ].side,  PLAYER_VS_SPRAY[ this.playerPosition ].step ) ) {
                    mySprayFound = true;
                    this.sprays[i].counter = 0;
                    break;
                }
            }

            if( ! mySprayFound ) {
                let myPosition = this.playerPosition > 4;
                let mySide = [ 5, 6, 0 ].indexOf( this.playerPosition ) !== NO_INDEX_OF ? LEFT : RIGHT;
                let myStep = [ 5, 9 ].indexOf( this.playerPosition ) !== NO_INDEX_OF ? 1 : 0;
                this.sprays.push( new Spray( this, myPosition, mySide, myStep ) );
            }
        }
    }

    onSpraysEffects() {
        let myDeadSprays = [];
        let myDeadBugs = [];
        let myDeadSpiders = [];
        for ( let i = 0; i < this.sprays.length; i++) {
            let myKilledBugs = this.sprays[ i ].isKillingBugs();
            myDeadBugs = myDeadBugs.concat( myKilledBugs );

            let myKilledSpider = this.sprays[ i ].isKillingSpider();
            if( myKilledSpider !== SPIDER_NO_KILL ) {
                if( myKilledSpider > 0 ) {
                    myDeadSpiders.push( myKilledSpider );
                }
                else {
                    this.onSpiderBackward();
                }
            }
            if( this.sprays[ i ].onEffectAndShouldDie() ) {
                myDeadSprays.push( i );
            }
        }

        this.sprays = this.sprays.filter( (value, index) => {
            return myDeadSprays.indexOf( index ) === NO_INDEX_OF;
        });

        this.bugs = this.bugs.filter( (value, index) => {
            if( myDeadBugs.indexOf( value.index ) === NO_INDEX_OF ) {
                return true;
            }
            this.onBugDead( value );
            return false;
        });

        this.spiders = this.spiders.filter( (value, index) => {
            if( myDeadSpiders.indexOf( value.index ) === NO_INDEX_OF ) {
                return true;
            }
            this.onSpiderDead( value );
            return false;
        });
    }

    onSpiderSpawn() {
        if( Math.random() > GAME_MODE_SPIDER_SPAWN ) {
            let mySide = ( Math.random() >= 0.5 ) ? LEFT : RIGHT;
            let myIsCanSpawn = true;
            for( let i = 0; i < this.spiders.length; i++ ) {
                if( this.spiders[i].side === mySide && this.spiders[i].pos !== 4 ) {
                    myIsCanSpawn = false;
                    break;
                }
            }

            if( myIsCanSpawn ) {
                this.spiders.push({
                    pos: 0,
                    side: mySide,
                    counter: 0,
                    index: this.indexSpiders++
                });
            }
        }
    }

    onBugSpawn() {
        if( Math.random() > GAME_MODE_BUG_SPAWN ) {
            if( this.bugs.length === 0 || ( this.bugs.length !== 0 && this.bugs[ this.bugs.length - 1 ].pos !== 0 ) ) {
                this.bugs.push({
                    pos: 0,
                    side: (Math.random() >= 0.5) ? LEFT : RIGHT,
                    counter: 0,
                    index: this.indexBugs++
                });
            }
        }
    }

    onSpidersMove() {
        let myMaxPosition = MONSTER_MAX_POS;
        for ( let i = this.spiders.length - 1; i >= 0; i-- ) {
            if( ++this.spiders[i].counter % ( ( this.mode === GAME_MODE_A ) ? GAME_MODE_A_MONSTER_MOVE_COUNTER : GAME_MODE_B_MONSTER_MOVE_COUNTER ) === 0 ) {
                let myNextPosition = this.spiders[i].pos + 1;

                if( myNextPosition === MONSTER_MAX_POS) {
                    //Flower dead ! :(
                    this.onFlowerEaten( false, this.spiders[i].side );
                }

                if( myNextPosition < myMaxPosition ) {
                    this.spiders[i].pos = myNextPosition;
                }
                myMaxPosition = myNextPosition;
            }
        }
    }

    onBugDead( bug ) {
        switch( bug.pos ) {
            case 4 :
                this.onScoreAdding( 3 );
                break;
            case 3:
                this.onScoreAdding( 2 );
                break;
            default:
                this.onScoreAdding( 1 );
                break;
        }
    }

    onSpiderDead( spider ) {
        if( spider.pos === 4 ) {
            this.onScoreAdding( 3 );
        }
        else {
            this.onScoreAdding( 1 );
        }
    }

    onSpiderBackward() {
        this.onScoreAdding( 1 );
    }

    onBugsMove() {
        let myMaxPosition = MONSTER_MAX_POS;
        for ( let i = 0; i < this.bugs.length; i++) {
            if( ++this.bugs[i].counter % ( ( this.mode === GAME_MODE_A ) ? GAME_MODE_A_MONSTER_MOVE_COUNTER : GAME_MODE_B_MONSTER_MOVE_COUNTER ) === 0 ) {
                let myNextPosition = this.bugs[i].pos + 1;

                if( myNextPosition === MONSTER_MAX_POS) {
                    //Flower dead ! :(
                    this.onFlowerEaten( true, this.bugs[i].side );
                }

                if( myNextPosition < myMaxPosition ) {
                    this.bugs[i].pos = myNextPosition;
                }
                myMaxPosition = myNextPosition;
            }
        }
    }

    /**
     *
     * @param position true = top, false = bottom
     * @param side true = left, false = right
     */
    onFlowerEaten( position, side ) {
        if( position ) {
            this.deadFlower = ( side === LEFT ) ? 0 : 1;
        }
        else {
            this.deadFlower = ( side === LEFT ) ? 2 : 3;
        }

        this.bugs = [];
        this.spiders = [];
        this.sprays = [];

        if( ++this.misses === FLOWER_MAX_DEAD ) {
            this.onEndGame()
        }
    }

    onEndGame() {
        if( this.interval !== 0 ) {
            window.clearInterval( this.interval );
            this.misses = 0;
            this.bugs = [];
            this.spiders = [];
            this.sprays = [];
            this.deadFlower = FLOWER_NOT_DEAD;

            let myPromptResult = prompt('Please enter your pseudo :)', this.registredPseudo );
            if( myPromptResult !== null ) {
                this.registredPseudo = myPromptResult;
                this.ws.sendScore( this.registredPseudo, this.score, this.mode );
            }
            this.mode = undefined;
        }
    }

    onScoreAdding( value ) {
        this.score += value;
        if( ! this.isScoreUpperThan300 && this.score >= 300 ) {
            this.isScoreUpperThan300 = true;
            if( this.misses === 0 ) {
                this.score += this.score;
            }
            else {
                this.misses = 0;
            }
        }

        if( this.score === SCORE_MAX ) {
            this.onEndGame();
        }
    }
}
