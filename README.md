# Green House online

[Gitlab](https://gitlab.com/servuc/green-house)

## Just a funky project

This project is a "pseudo" reverse engineering of Green House Game & Watch.

## Usage

Update `server.js` with your URL. Don't touch port.

    cd server
    node index.js
    php -S 0.0.0.0:FREE_PORT_THAT_YOU_PREFER
    
Use PHP is dirty ..., really ? It's in TODO

## TODO

 - [ ] Optimized Dockerfile
 - [ ] Stop using PHP, prefer package `http`
 - [ ] Comment code

## Links

https://www.oldiesrising.com/AmanoSkin/oldiesrisingscreensV2.php?titre=Green%20House&cons=28

http://www.intheattic.co.uk/greenhouse.htm

## Disclaimer

All images, gameplay, logo, description in game are copied from original Game&Watch Green House, property of Nintendo.

This project is just is just for JS education purpose :)
